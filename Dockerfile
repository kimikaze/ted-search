FROM maven:3-jdk-8 as base
COPY . /app
WORKDIR /app
RUN mvn verify

FROM openjdk:8-jre
COPY --from=base /app/target /app
COPY --from=base /app/application.properties /app/application.properties
WORKDIR /app
CMD ["sleep 3000"]
#ENTRYPOINT ["java", "-jar", "embedash-1.1-SNAPSHOT.jar", "--spring.config.location=./application.properties"]